package com.example.RightOClock.Utils;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

public class Utils {
    public static Map<String, LocalDateTime> dataSource1 = new HashMap<>();
    public static Map<String, LocalDateTime> dataSource2 = new HashMap<>();
    public static Map<String, LocalDateTime> dataSource3 = new HashMap<>();
    public static Map<String, LocalDateTime> dataSource4 = new HashMap<>();
    public static Map<String, LocalDateTime> dataSource5 = new HashMap<>();

    public static LocalDateTime meetingTime1;
    public static LocalDateTime meetingTime2;
    public static LocalDateTime meetingTime3;
    public static LocalDateTime meetingTime4;
    public static LocalDateTime meetingTime5;

    static {
        dataSource1.put("milan1@gmail.com",LocalDateTime.of(2021,2,23,5,30));
        dataSource1.put("milan2@gmail.com",LocalDateTime.of(2021,2,23,5,22));
        dataSource1.put("milan3@gmail.com",LocalDateTime.of(2021,2,23,5,32));
        dataSource1.put("milan4@gmail.com",LocalDateTime.of(2021,2,23,5,34));
        dataSource1.put("milan5@gmail.com",LocalDateTime.of(2021,2,23,5,31));
        dataSource1.put("pratik1@gmail.com",LocalDateTime.of(2021,2,23,5,29));
        dataSource1.put("pratik2@gmail.com",LocalDateTime.of(2021,2,23,5,23));
        dataSource1.put("pratik3@gmail.com",LocalDateTime.of(2021,2,23,5,31));
        dataSource1.put("pratik4@gmail.com",LocalDateTime.of(2021,2,23,5,33));
        dataSource1.put("pratik5@gmail.com",LocalDateTime.of(2021,2,23,5,30));
        meetingTime1 = LocalDateTime.of(2021,2,23,5,30);

        dataSource2.put("milan1@gmail.com",LocalDateTime.of(2021,2,23,5,21));
        dataSource2.put("milan2@gmail.com",LocalDateTime.of(2021,2,23,5,32));
        dataSource2.put("milan3@gmail.com",LocalDateTime.of(2021,2,23,5,34));
        dataSource2.put("milan4@gmail.com",LocalDateTime.of(2021,2,23,5,45));
        dataSource2.put("milan5@gmail.com",LocalDateTime.of(2021,2,23,5,31));
        dataSource2.put("pratik1@gmail.com",LocalDateTime.of(2021,2,23,5,33));
        dataSource2.put("pratik2@gmail.com",LocalDateTime.of(2021,2,23,5,32));
        dataSource2.put("pratik3@gmail.com",LocalDateTime.of(2021,2,23,5,27));
        dataSource2.put("pratik4@gmail.com",LocalDateTime.of(2021,2,23,5,35));
        dataSource2.put("pratik5@gmail.com",LocalDateTime.of(2021,2,23,5,33));
        meetingTime2 = LocalDateTime.of(2021,2,23,5,30);

        dataSource3.put("milan1@gmail.com",LocalDateTime.of(2021,2,23,5,30));
        dataSource3.put("milan2@gmail.com",LocalDateTime.of(2021,2,23,5,30));
        dataSource3.put("milan3@gmail.com",LocalDateTime.of(2021,2,23,5,37));
        dataSource3.put("milan4@gmail.com",LocalDateTime.of(2021,2,23,5,21));
        dataSource3.put("milan5@gmail.com",LocalDateTime.of(2021,2,23,5,33));
        dataSource3.put("pratik1@gmail.com",LocalDateTime.of(2021,2,23,5,35));
        dataSource3.put("pratik2@gmail.com",LocalDateTime.of(2021,2,23,5,38));
        dataSource3.put("pratik3@gmail.com",LocalDateTime.of(2021,2,23,5,39));
        dataSource3.put("pratik4@gmail.com",LocalDateTime.of(2021,2,23,5,30));
        dataSource3.put("pratik5@gmail.com",LocalDateTime.of(2021,2,23,5,31));
        meetingTime3 = LocalDateTime.of(2021,2,23,5,30);

        dataSource4.put("milan1@gmail.com",LocalDateTime.of(2021,2,23,5,43));
        dataSource4.put("milan2@gmail.com",LocalDateTime.of(2021,2,23,5,33));
        dataSource4.put("milan3@gmail.com",LocalDateTime.of(2021,2,23,5,34));
        dataSource4.put("milan4@gmail.com",LocalDateTime.of(2021,2,23,5,36));
        dataSource4.put("milan5@gmail.com",LocalDateTime.of(2021,2,23,5,37));
        dataSource4.put("pratik1@gmail.com",LocalDateTime.of(2021,2,23,5,33));
        dataSource4.put("pratik2@gmail.com",LocalDateTime.of(2021,2,23,5,35));
        dataSource4.put("pratik3@gmail.com",LocalDateTime.of(2021,2,23,5,35));
        dataSource4.put("pratik4@gmail.com",LocalDateTime.of(2021,2,23,5,36));
        dataSource4.put("pratik5@gmail.com",LocalDateTime.of(2021,2,23,5,39));
        meetingTime4 = LocalDateTime.of(2021,2,23,5,30);

        dataSource5.put("milan1@gmail.com",LocalDateTime.of(2021,2,23,5,31));
        dataSource5.put("milan2@gmail.com",LocalDateTime.of(2021,2,23,5,31));
        dataSource5.put("milan3@gmail.com",LocalDateTime.of(2021,2,23,5,34));
        dataSource5.put("milan4@gmail.com",LocalDateTime.of(2021,2,23,5,37));
        dataSource5.put("milan5@gmail.com",LocalDateTime.of(2021,2,23,5,29));
        dataSource5.put("pratik1@gmail.com",LocalDateTime.of(2021,2,23,5,31));
        dataSource5.put("pratik2@gmail.com",LocalDateTime.of(2021,2,23,5,32));
        dataSource5.put("pratik3@gmail.com",LocalDateTime.of(2021,2,23,5,29));
        dataSource5.put("pratik4@gmail.com",LocalDateTime.of(2021,2,23,5,36));
        dataSource5.put("pratik5@gmail.com",LocalDateTime.of(2021,2,23,5,40));
        meetingTime5 = LocalDateTime.of(2021,2,23,5,30);
    }
}
