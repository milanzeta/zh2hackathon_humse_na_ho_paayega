package com.example.RightOClock.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.util.Assert;

import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Component
public class UpdatePointsDao {

    @Autowired
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    private final TransactionTemplate transactionTemplate;

    @Autowired
    private final PlatformTransactionManager transactionManager;


    @Autowired
    public UpdatePointsDao(JdbcTemplate jdbcTemplate, PlatformTransactionManager transactionManager) {
        this.jdbcTemplate = jdbcTemplate;
        Assert.notNull(transactionManager, "The 'transactionManager' argument must not be null.");
        this.transactionTemplate = new TransactionTemplate(transactionManager);
        this.transactionManager = transactionManager;
    }
    ExecutorService executor = Executors.newFixedThreadPool(10);

    public CompletableFuture<Void> updatePoints(Map<String, Integer> emailsWithPoints){
        CompletableFuture<Void> cf = new CompletableFuture<>();
        CompletableFuture.supplyAsync(() ->transactionTemplate.execute(new TransactionCallbackWithoutResult() {
            protected void doInTransactionWithoutResult(TransactionStatus status) {

                String sql = "UPDATE user_points SET points = points + ? WHERE email = ?";
                for (Map.Entry<String,Integer> userPoints : emailsWithPoints.entrySet()) {
                    jdbcTemplate.update(sql, userPoints.getValue(),userPoints.getKey());
                }

            }
        }),executor).whenComplete((result,exc) -> {
            if(exc!=null){
                cf.completeExceptionally(new RuntimeException("Database error"));
            }
            cf.complete(null);
        });
        return cf;
    }
}
