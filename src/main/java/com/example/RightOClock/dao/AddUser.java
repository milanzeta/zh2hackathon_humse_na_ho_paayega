package com.example.RightOClock.dao;


import com.example.RightOClock.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Component
public class AddUser {
    @Autowired
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public AddUser(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
    ExecutorService executor = Executors.newFixedThreadPool(10);
    public CompletableFuture<Integer> insertUser(User user) {
        String sql = "" +
                "INSERT INTO user_points ( firstName,secondName,email,points ) VALUES (?,?,?,?)";
        CompletableFuture<Integer> cf = new CompletableFuture<>();
        CompletableFuture.supplyAsync(() -> jdbcTemplate.update(
                sql,user.getFirstName(),user.getSecondName(),user.getEmail(),0
        ),executor).whenComplete((result,exc) -> cf.complete(result));

        return cf;
    }
}
