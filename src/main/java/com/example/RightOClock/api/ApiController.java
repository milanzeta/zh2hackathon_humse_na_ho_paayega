package com.example.RightOClock.api;
import com.example.RightOClock.Utils.Utils;
import com.example.RightOClock.model.User;
import com.example.RightOClock.service.UpdateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.concurrent.CompletionStage;

@RestController
@RequestMapping("api")
public class ApiController {
    @Autowired
    public UpdateService updateService;

    @Autowired
    public ApiController(UpdateService updateService) {
        this.updateService = updateService;
    }

    @PostMapping(path = "/user")
    public CompletionStage<Integer> addUser(@RequestBody User user) {
        return updateService.addUser(user);
    }

    @PostMapping(path = "/points")
    public void calculateMeetingPoints() {
        updateService.calculateMeetingPoints(Utils.meetingTime1, Utils.dataSource1);
        updateService.calculateMeetingPoints(Utils.meetingTime2, Utils.dataSource2);
        updateService.calculateMeetingPoints(Utils.meetingTime3, Utils.dataSource3);
        updateService.calculateMeetingPoints(Utils.meetingTime4, Utils.dataSource4);
        updateService.calculateMeetingPoints(Utils.meetingTime5, Utils.dataSource5);
    }

}
