package com.example.RightOClock.service;

import com.example.RightOClock.dao.AddUser;
import com.example.RightOClock.dao.UpdatePointsDao;
import com.example.RightOClock.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;


@Service
public class UpdateService {
    @Autowired
    public final UpdatePointsDao updatePointsDao;
    @Autowired
    public final AddUser addUserDao;

    @Autowired
    public UpdateService(UpdatePointsDao updatePointsDao, AddUser addUserDao) {
        this.updatePointsDao = updatePointsDao;
        this.addUserDao = addUserDao;
    }

    public CompletionStage<Void> calculateMeetingPoints(LocalDateTime meetingTime, Map<String, LocalDateTime> joiningInfo) {
        Map<String, Integer> points = new HashMap<>();
        for(Map.Entry<String, LocalDateTime> user : joiningInfo.entrySet()) {
            LocalDateTime userJoiningTime = user.getValue();
            LocalDateTime oneMinuteLater = meetingTime.plusMinutes(1);
            LocalDateTime twoMinutesLater = meetingTime.plusMinutes(2);

            if(!userJoiningTime.isAfter(meetingTime)) {
                points.put(user.getKey(), 200);
            }
            else if(!userJoiningTime.isAfter(oneMinuteLater)) {
                points.put(user.getKey(), 50);
            }
            else if(!userJoiningTime.isAfter(twoMinutesLater)) {
                points.put(user.getKey(), 25);
            }
        }
        return updatePointsDao.updatePoints(points);
    }

    public CompletableFuture<Integer> addUser(User user) {
        return addUserDao.insertUser(user);
    }
}
